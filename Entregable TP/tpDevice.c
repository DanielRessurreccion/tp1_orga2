/* tpDevice.c */

#include <linux/fs.h>
#include <linux/string.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/uaccess.h>


/* Prototipos */

#define Nombre_Driver "tpDevice"
#define Cant_Max_Carac	512


int init_module(void);
void cleanup_module(void);
static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);


/* Variables Globales */

static int Mayor = 100;
static int Dispositivo_Abierto = 0;
static char Mi_Buffer[Cant_Max_Carac];
static unsigned long Cant_Carac_Entrada = 0;

 
static struct file_operations fops = {
	.read = device_read,
	.write = device_write,
	.open = device_open,
	.release = device_release
};


/* Carga del módulo */

int init_module(void){
	int resultado = register_chrdev(Mayor, Nombre_Driver, &fops);
	if (Mayor < 0) {
		printk(KERN_ALERT "Registro Faliido \n");
		return Mayor;
	}
	printk(KERN_INFO "Se registró correctamente el módulo %s\n", Nombre_Driver);
	printk(KERN_INFO "Cree un archivo en /dev con 'mknod /dev/%s c %d 0'.\n", Nombre_Driver, Mayor);	
	return resultado;
}


/* Descarga del módulo */

void cleanup_module(void) {
	unregister_chrdev(Mayor, Nombre_Driver);
	printk(KERN_INFO "Se eliminó correctamente el módulo %s\n", Nombre_Driver);
}


/* Abre el módulo */

static int device_open(struct inode *inode, struct file *file){
	if(Dispositivo_Abierto){
		return -EBUSY;
	}
	Dispositivo_Abierto++;
	try_module_get(THIS_MODULE);
	return 0;
}


/* Cierra el módulo */

static int device_release(struct inode *inode, struct file *file){	
	Dispositivo_Abierto--;
	module_put(THIS_MODULE);
	return 0;
}


/* Intenta leer el archivo en /dev */

static ssize_t device_read(struct file *filp, char *buffer, size_t length, loff_t * offset) {
	static int finalizado = 0;		
	if (finalizado) {
		printk(KERN_INFO "Archivo leido exitosamente\n");
		finalizado = 0;
		return 0;
	}
	if (copy_to_user(buffer, Mi_Buffer, Cant_Carac_Entrada)) {	
		return -EFAULT;
	}
	finalizado = 1;	
	printk(KERN_INFO "Fueron leidos %lu bytes\n", Cant_Carac_Entrada);
	return Cant_Carac_Entrada;	
}


/* Intenta escribir en el archivo en /dev */

static ssize_t device_write(struct file *filp, const char *buff, size_t len, loff_t *off) {
	Cant_Carac_Entrada = len;
	if (Cant_Carac_Entrada > Cant_Max_Carac ) {
		Cant_Carac_Entrada = Cant_Max_Carac;
	}		
	memset(Mi_Buffer, 0, Cant_Max_Carac);
	if (copy_from_user(Mi_Buffer, buff, Cant_Carac_Entrada)) {
		return -EFAULT;
	}
	printk(KERN_INFO "Fueron escritos %lu bytes\n", Cant_Carac_Entrada);
	return Cant_Carac_Entrada;
}



MODULE_LICENSE (" GPL ");
MODULE_AUTHOR (" Daniel Ressurrección ");
MODULE_DESCRIPTION (" TP CharDevice - Organización del Computador 2 ");
